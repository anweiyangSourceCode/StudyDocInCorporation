/**
 * 这个是express搭建的http服务器，其实早用express框架搭建的话，可能也没有之前那么多的问题了
 * 不过用http对象来搭建也让我学到了不少的东西，比如chrome浏览器的那个ajax请求不对的问题
 */

var express = require('express');
var queryString = require('querystring');
var routes = require('./routes');
var dbservice = require('./dbservice');
var app = express();

app.use(express.static(__dirname));

app.set('view engine','ejs');
//app.configure(function(){
//	app.set('view engine','ejs');
//	app.use(express.bodyParser());
//	app.use(express.methodOverride());
//	app.use(app.router);
//	app.use(express.static(_dirname+'public'));
//});

app.get('/',routes.index);


app.get('/index',routes.index);

app.get('/find',function(req,res){
	
	var queryObj = dbservice.findAll();
	queryObj.exec(function(err,contacts){
		//contacts是数据库返回的数据
		console.log("return all is in ")
		console.log(contacts);
		if(err)
			throw err;
		//下面的工作是用response对象返回我们得到的对象
		var dataStr = JSON.stringify(contacts);
		res.writeHead(200,{"Content-Type": "text/plain","Access-Control-Allow-Origin":req.headers.origin});
		
		res.write(dataStr);	
		res.end();
		
	});
});

app.post('/insert',function(req,res){
	var postdata = '';
	
	req.on('data',function(data){
        postdata += data;
        //在这里解析postdata,分析出其中的三种数据name，number，address
        var contact = queryString.parse(postdata);
        dbservice.add(contact.name,contact.number,contact.address);
        
        console.log(postdata);
        console.log(contact);
    
	});
	
	 req.on("end",function(){
		 response.writeHead(200,{"Content-Type": "text/plain","Access-Control-Allow-Origin":req.headers.origin});
		 //这里有一个需要注意的问题：下面这句代码，在向chrome浏览器返回数据的时候可能报错。Access-Control-Allow-Origin  这个相关的错。
		 //原因就是某些浏览器不允许跨域访问。具体我并不是特别明白，但是大概可以理解成，自己可以给别人发请求，但是如果response的header中没有Access-Control-Allow-Origin
		 //这个字段的话，就不接收返回的数据。
		 //		 res.writeHead(200,{"Content-Type": "text/plain"});
         res.write(JSON.stringify(postdata));
         res.end();
     })
	
	
});





app.listen(1337);