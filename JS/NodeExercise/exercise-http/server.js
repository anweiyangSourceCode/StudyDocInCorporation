/**
 * node工程的试验
 */
/**/



/**/
var http = require("http");
var url = require("url");
var queryString = require("querystring");
//操作db的js
var dbservice = require("./dbservice");
var req;
var res;
http.createServer(function(request,response){
	req = request;
	res = response;
	//现在这么做：分析客户端的url传递过来的信息，如果是query，那么返回db中的所有查询结果
	//如果是insert，那么向db中添加一条数据
	//如果是update，那么更新db中的一条数据
	//如果是delete,那么删除db中的一条数据（delete的时候需要指定删除的id）
	
	//解析请求url:
	//首先解析url中的查询部分，如果匹配insert，添加，匹配update，更新，匹配delete，删除，匹配find，返回所有查询出来的对象。
	var parser = url.parse(request.url);
	console.log(request);
	console.log(parser.pathname);
	//如何实现Restful API，我在这里想到的方法是：
	//首先，可以用pathname来确定做什么操作，然后再利用后面带的参数来确定如何操作。
	/*get与post的分别处理暂时不考虑
	 * if(request.method === 'GET'){
		switch(parser.pathname){
		
		}
	}else if(request.method ==='POST'){
		
	}*/
	switch(parser.pathname){
		case '/insert':
			//解析传送过来的数据
			
//			dbservice.add('insertedPerson','1234567890','大连');
//			sendToClient('insert',response);
			//下面的代码处理post数据
			var postdata = "";
	        request.on("data",function(postchunk){
	        	//postchunk就是post请求中携带的数据，但是注意，这个数据的格式,并不是JSON数据块，而是二进制的
	        	//我们这里用postdata将postchunk变成字符串格式，但是注意，组装之后的postdata并不是Json格式的
	        	//而是这种格式：name=DonaldDuck&number=77777777777&address=Duckburg
	        	//做到这里让我深刻感觉到，如果前台能直接发JSON该多好啊。
//	        	console.log(postchunk);
	            postdata += postchunk;
	            //在这里解析postdata,分析出其中的三种数据name，number，address
	            var contact = queryString.parse(postdata);
	            dbservice.add(contact.name,contact.number,contact.address);
	            
	            console.log(postdata);
	            console.log(contact);
	        })
	        //POST结束输出结果
	        request.on("end",function(){
//	            var params = query.parse(postdata);
//	            params['fruit'] = compute(params);
	        	response.writeHead(200,{"Content-Type": "text/plain","Access-Control-Allow-Origin":req.headers.origin});
	            response.write(JSON.stringify(postdata));
	            response.end();
	        })
			break;
		case '/update':
			dbservice.updateNameById('54f1557942b5dbd1104a7ad3','update0');
			sendToClient('update',response);
			break;
		case '/delete':
			dbservice.delByname('hello');
			sendToClient('deleted',response);
			break;
		case '/find':
			var queryObj = dbservice.findAll();
			queryObj.exec(returnAll);
//			console.log("data is"+data);
//			var data2 = JSON.stringify(data);
//			console.log(data2);
//			sendToClient('find',response);
			break;
		default:
			sendToClient('Hello world',response);
			break;
	}
//	parser.query
	
	
	
	
	
	
	
	console.log(request.method);
	console.log(request.httpVersion);

}).listen(8888);

var sendToClient = function(message,response){
	
	response.writeHead(200,{"Content-Type": "text/plain","Access-Control-Allow-Origin":req.headers.origin});
	response.write(message); 

    response.end(); 
}

//findAll的数据库回调函数,返回数据库中的所有查询结果
var returnAll = function(err,contacts){
	//contacts是数据库返回的数据
	console.log("return all is in ")
	console.log(contacts);
	if(err)
		throw err;
	//下面的工作是用response对象返回我们得到的对象
	var dataStr = JSON.stringify(contacts);
	res.writeHead(200,{"Content-Type": "text/plain","Access-Control-Allow-Origin":req.headers.origin});
	
	res.write(dataStr);	
	res.end();
	
}
/*
//测试与mongodb的链接(没有使用mongoose)，并操作数据
var mongodb = require('mongodb');
var server = new mongodb.Server('127.0.0.1',27017,{});
var client = new mongodb.Db('mydatabase',server,{});

client.open(function(err){
	  if(err) throw err;
	  client.collection('test',function(err,collection){
	    if(err) throw err;

	    //这里放置各种mongodb的操作语句...*
	    collection.insert(
	    		{"title":"i like cake","body":"it is quite good"},
	    		{safe:true},
	    		function(err,documents){
	    			if(err) throw err;
	    			console.log('Document ID is:'+documents[0]._id);
	    		}
	    );
	  });
	});
*/
/*
//利用mongoose连接mongodb服务器,并操作数据
var mongoose = require('mongoose');
var db = mongoose.connect('mongodb://localhost/mydatabase');

//注册schema
var Schema = mongoose.Schema;
var Tasks = new Schema({
	project:String,
	description:String
});
mongoose.model('Task',Tasks);

var Task = mongoose.model('Task');
var task = new Task();
task.project = 'bikeshed';
task.description = 'Paint the bikeshed red.';
task.save(function(err){
	if(err) throw err;
	console.log("Task saved.");
});
*/

/*
//利用mongoose连接mongodb服务器,插入需要的数据
var mongoose = require('mongoose');
var db = mongoose.connect('mongodb://localhost/mydatabase');

//注册schema
var Schema = mongoose.Schema;
var Tasks = new Schema({
	project:String,
	description:String
});
mongoose.model('Task',Tasks);

var Task = mongoose.model('Task');
var task = new Task();
task.project = 'bikeshed';
task.description = 'Paint the bikeshed red.';
task.save(function(err){
	if(err) throw err;
	console.log("Task saved.");
});
*/
//var dbservice = require("./dbservice");
//dbservice.add('zhangsan','123123432132','street1');
//dbservice.add('wangwu','123123432','street2');
//dbservice.add('lisi','123123432132','street3');
//dbservice.add('hello','123123432132','street3');
//dbservice.add('你好','123123432132','中国');
//dbservice.add('大家好2','123123432132','street3');
//dbservice.add('大家好3','123123432132','street3');
//dbservice.add('大家好4','123123432132','street3');
//
//dbservice.find('zhangsan');
//dbservice.findAll();
//dbservice.del('54f1557942b5dbd1104a7ad1');
//dbservice.update("54f1557942b5dbd1104a7ad3");



